# Plugin download

::: tip

MuiPlayer uses the GPL open source license agreement. Before that, please be sure to understand the license description of the agreement and comply with the legal conditions of the agreement.

You can get the plugin release package from the open source address [Github](https://github.com/muiplayer/hello-muiplayer).

[![star](https://gitee.com/muiplayer/hello-muiplayer/badge/star.svg?theme=dark)](https://gitee.com/muiplayer/hello-muiplayer/stargazers)

[![GitHub Repo stars](https://img.shields.io/github/stars/muiplayer/hello-muiplayer?style=social)](https://github.com/muiplayer/hello-muiplayer/stargazers)


::: 



## Value added services

The professional version of the mobile terminal and PC terminal extension plug-ins have taken hundreds of days and has been continuously updated to fix hundreds of key bugs. Please download the latest version before using it. One download is free for life! Usually the plugin is to keep the latest version.

| plugin type                                        | Professional function                                        | Price(US)<br />invoice     | purchase address                       |
| :------------------------------------------------- | :----------------------------------------------------------- | :------------------------- | :------------------------------------- |
| Mobile <br />Extension plugin professional edition | 1. Get the professional version of the mobile playback extension plug-in ✔️ <br />2. Free access to future update versions ✔️<br />3. Compatible with Iphone, Androi models and uniapp webview ✔️<br />4. Support Uniapp Webview and use in H5+ APP ✔️<br />5. Supports simplified Chinese, traditional Chinese, and English three language configurations ✔️<br />6. Professional technical development support ✔️<br />7. Eligible to enter the VIP group ✔️<br />8. Customizable player development ✔️ | / <br /> | / |
| PC<br />Extension plugin professional edition | 1. Get the professional version of the mobile playback extension plug-in ✔️ <br />2. Free access to future update versions ✔️<br />3. Supports simplified Chinese, traditional Chinese, and English three language configurations ✔️<br />4. Professional technical development support ✔️<br />5. Eligible to enter the VIP group ✔️<br />6. Customizable player development ✔️ | /<br /> | / |




